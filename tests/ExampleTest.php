<?php

use App\Survey;

class ExampleTest extends TestCase {

	/**
	 * Default preparation for each test
	 *
	 */
	public function setUp()
	{
	    parent::setUp();
	    $this->prepareForTests();
	}

	/**
	 * Migrates the database.
	 * This will cause the tests to run quickly.
	 */
	private function prepareForTests()
	{
		// set environment db
		putenv('DB_DRIVER=testing_db');

		// initialize testing db
	    Artisan::call('migrate');
	}

	public function test_surveys_page_exists()
	{
		$this->call('GET', '/surveys');

		$this->assertResponseOk();
	}

	public function test_it_stores_a_new_survey_object()
	{
		$response = $this->call('POST', 'surveys/store', ['survey_name' => 'test']);

		$surveys = Survey::all();

		$this->assertCount(1, $surveys);
	}
}
