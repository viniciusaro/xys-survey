@extends('app')

@section('content')
<div class="row">

	<div class="col-md-10 col-md-offset-1">

		{!! Form::open(['action' => 'SurveyDashboardController@store']) !!}

		<div class="form-group">
			{!! Form::label('Survey name:') !!}
			{!! Form::text('survey_name', null, ['class' => 'form-control']) !!}
		</div>

		<div class="questions">

			<div class="question">

				<div class="panel panel-default">

				<div class="panel-heading question-heading">Question 1:</div>

				<div class="panel-body">
						
					<div class="form-group">
						{!! Form::text('question_title[0]', null, ['class' => 'form-control question-title']) !!}
					</div>

					<div class="question-options">

						<div class="question-option">

						<div class="form-group">
							{!! Form::label('Option 1:') !!}
							{!! Form::text('question_option[0][0]', null, ['class' => 'form-control option-body']) !!}
						</div>

						</div>

					</div>

					<button type="button" class="btn btn-default form-control add-option" question="0">+ Option</button>

				</div>

				</div>
	
			</div>

		</div>

		<button type="button" class="btn btn-warning form-control" id="add-question">+ Question</button>
		<hr/>

		<div class="form-group">
			{!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
		</div>

		
		{!! Form::close() !!}	
		
	</div>
</div>
@endsection

@section('footer')

<script type="text/javascript">
	
	$('#form').submit(false);

	$('#add-question').on('click', function(e) 
	{
		var questionHTML = $('.question').first().clone();
		var questionCount = $('.question').length;

		questionHTML.find('.question-title').val('');
		questionHTML.find('.question-title').attr('name', 'question_title['+questionCount+']');
		questionHTML.find('.question-heading').html('Question '+(questionCount+1)+':');
		questionHTML.find('.add-option').attr('question', questionCount);

		var questionOptionsHTML = questionHTML.find('.question-options');
		var questionOptionHTML = questionHTML.find('.question-option').first();
		questionOptionHTML.find('.option-body').attr('name', 'question_option['+questionCount+'][0]');
		questionOptionHTML.find('.option-body').val('');
		questionOptionsHTML.html(questionOptionHTML);

		$('.questions').append(questionHTML);
	});


	$(document).on('click', '.add-option', function(e)
	{
		var optionContainer = $(this).siblings('.question-options');
		var questionIndex 	= $(this).attr('question');
		var optionCount 	= optionContainer.find('.question-option').length;
		var optionHTML 		= optionContainer.find('.question-option').first().clone();

		optionHTML.find('label').html('Option '+(optionCount+1)+':');
		optionHTML.find('.option-body').val('');
		optionHTML.find('.option-body').attr('name', 'question_option['+questionIndex+']['+optionCount+']');

		optionContainer.append(optionHTML);
	});

</script>

@endsection
