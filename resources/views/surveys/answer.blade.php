@extends('app')

@section('content')
<div class="container">
	
	<div class="row">

		<div class="col-md-10 col-md-offset-1">

			<div class="panel panel-default">

				<div class="panel-heading">{{ $survey->name }}</div>

			</div>

			{!! Form::open(['action' => 'SurveyDashboardController@storeAnswer']) !!}

			{!! Form::hidden('id', $survey->id) !!}

			@foreach ($survey->questions as $index => $question)

			<div class="panel panel-default">

				<div class="panel-heading">{{ $index + 1}} - {{$question->title }}</div>

				<ul class="list-group">

					@foreach ($question->options as $option)
					
					<li class="list-group-item">
					  	{!! Form::radio('question_id['.$question->id.']', $option->id, null) !!} {{ $option->body }}
					</li>
					
					@endforeach

				</ul>
			
			</div>

			@endforeach

			{!! Form::submit('Send', ['class' => 'btn btn-warning form-control']) !!}

			{!! Form::close() !!}

		</div>
	</div>
</div>
@endsection
