@extends('app')

@section('content')
<div class="container">

	<div class="row">

		<div class="col-md-10 col-md-offset-1">
			
			<div class="panel panel-default">

				<div class="panel-heading"><a href={{ action("SurveyDashboardController@create") }}>New Survey</a></div>

			</div>

			<div class="panel panel-default">

				<div class="panel-heading">Current Surveys</div>

				<ul class="list-group">

					@foreach ($surveys as $survey)

						<li class="list-group-item">
							
						  	<a href={{ action("SurveyDashboardController@show", [$survey->id]) }}>{{ $survey->name }}</a>

						  	<span class="pull-right">

						  		<a href={{ action("SurveyDashboardController@delete", [$survey->id]) }}>

									<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
								</a>
						  	</span>
					  	</li>
					@endforeach
				</ul>
			</div>

			<div class="panel panel-default">
				
				<div class="panel-heading">Statistics</div>

				@include('surveys.dashboard.statistics')

			</div>
		</div>
	</div>
</div>
@endsection
