<ul class="list-group">

	@foreach ($surveys as $survey)

		<li class="list-group-item">
			
		  	{{ $survey->name }}

		  	<ul>
		  	@foreach ($survey->questions as $question)
				<li>
				  	{{ $question->title }}
				  	
				  	<hr/>

				  	<ul>
				  	@foreach ($question->options as $option)
						<li>

						  	<?php $percentage = 0 ?>

						  	@if (array_key_exists($option->id, $answerInfo))
						  		
						  		<?php 
						  			$percentage = $answerInfo[$option->id]/$answerInfo['question_count'][$question->id];
						  			$percentage = round($percentage, 3)*100;
						  		?>

						  		{{ $option->body }} ({{ $answerInfo[$option->id] }} votos, {{$percentage}}%)
						  	@else 
						  		{{ $option->body }} (0 votos, 0%)
						  	@endif

						  	<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow={{ $percentage }} aria-valuemin="0" aria-valuemax="100" style={{ 'width:'.$percentage.'%' }}></div>
							</div>

					  	</li>
					@endforeach
					</ul>
			  	</li>
			@endforeach
			</ul>
	  	</li>
	@endforeach
</ul>