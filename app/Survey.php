<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model 
{

	/**
	 * Mass assignable fields - create(array)
	 */
	protected $fillable = [
		'name'
	];

	/**
	 * Defines the questions relationship
	 * 
	 * @return void
	 */
	public function questions() 
	{
		return $this->hasMany('App\Question');
	}
}
