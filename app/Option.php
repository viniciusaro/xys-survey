<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model {

	/**
	 * Mass assignable fields - create(array)
	 */
	protected $fillable = [
		'body'
	];

	/**
	 * Defines the questions relationship
	 * 
	 * @return void
	 */
	public function question() 
	{
		return $this->belongsTo('App\Question');
	}

	/**
	 * Defines the options relationship
	 * 
	 * @return void
	 */
	public function questionAnswer() 
	{
		return $this->belongsTo('App\QuestionAnswer');
	}
}
