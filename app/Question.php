<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model 
{

	/**
	 * Mass assignable fields - create(array)
	 */
	protected $fillable = [
		'title'
	];

	/**
	 * Defines the survey relationship
	 * 
	 * @return void
	 */
	public function survey()
    {
        return $this->belongsTo('App\Survey');
    }

    /**
	 * Defines the options relationship
	 * 
	 * @return void
	 */
	public function options() 
	{
		return $this->hasMany('App\Option');
	}

	/**
	 * Defines the options relationship
	 * 
	 * @return void
	 */
	public function questionAnswer() 
	{
		return $this->belongsTo('App\QuestionAnswer');
	}
}
