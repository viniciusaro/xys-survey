<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('surveys', 'SurveyDashboardController@index');
Route::post('surveys/store', 'SurveyDashboardController@store');
Route::post('surveys/storeAnswer', 'SurveyDashboardController@storeAnswer');

Route::get('surveys/create', 'SurveyDashboardController@create');
Route::get('surveys/delete/{id}', 'SurveyDashboardController@delete');
Route::get('surveys/{id}', 'SurveyDashboardController@show');