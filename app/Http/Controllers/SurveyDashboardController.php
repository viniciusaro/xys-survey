<?php namespace App\Http\Controllers;

use App\Survey;
use App\Question;
use App\Option;
use App\SurveyAnswer;
use App\QuestionAnswer;
use App\Http\Requests;
use Request;

class SurveyDashboardController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Survey Dashboard Controller
	|--------------------------------------------------------------------------
	*/

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$surveys = Survey::all();

		$answerInfo = QuestionAnswer::allAnswerInfo();

		return view('surveys.dashboard.main', compact('surveys', 'answerInfo'));
	}

	/**
	 * Create new survey
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('surveys.create');
	}

	/**
	 * Show survey details
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$survey = Survey::findOrFail($id);

		return view('surveys.answer', compact('survey'));
	}

	/**
	 * Show survey details
	 *
	 * @return Response
	 */
	public function delete($id)
	{
		$survey = Survey::findOrFail($id);

		$survey->delete();

		return redirect('surveys');
	}

	/**
	 * Save new survey
	 *
	 * @return Response
	 */
	public function storeAnswer()
	{
		$input = Request::all();

		foreach ($input['question_id'] as $question_id => $option_id) 
		{
			QuestionAnswer::create([
				'question_id' 		=> $question_id, 
				'option_id' 		=> $option_id]);
		}

		return redirect('surveys');
	}

	/**
	 * Save new survey
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Request::all();

		$survey = $this->storeSurvey($input['survey_name']);

		$this->storeQuestions($survey, $input['question_title'], $input['question_option']);

		return redirect('surveys');
	}

	/**
	 * Creates and return a new survey with name
	 *
	 * @var $name: the name of the survey 
	 *
	 * @return new survey
	 */
	public function storeSurvey($name) 
	{
		return Survey::create(['name' => $name]);
	}

	/**
	 * Creates and stores a set of questions for a specified survey
	 *
	 * @var $survey: survey associated with the questions
	 * @var $questionTitles: array of questions 
	 *
	 * @return void
	 */
	public function storeQuestions($survey, $questionTitles, $questionOptions) 
	{
		foreach ($questionTitles as $index => $questionTitle) 
		{
			$question = new Question(['title' => $questionTitle]);

			$survey->questions()->save($question);

			$options = $questionOptions[$index];

			foreach ($options as $questionOption) 
			{
				$option = new Option(['body' => $questionOption]);
				$question->options()->save($option);
			}
		}
	}
}