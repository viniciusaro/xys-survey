<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionAnswer extends Model {

	/**
	 * Mass assignable fields - create(array)
	 */
	protected $fillable = [
		'question_id',
		'option_id'
	];

	public function question() 
	{
		return $this->hasOne('App\Question');
	}

	public function option() 
	{
		return $this->hasOne('App\Option');
	}

	public static function allAnswerInfo()
	{
		// retreive all answers for all surveys
		$questionAnswers = QuestionAnswer::all();

		$answerInfo = [];
		$questionCount = [];

		/* formats the objects in array form
		(
			'option_id': number_of_votes
			...
			
			'question_count' : (
				'question_id': total_votes
				...
			)
		)
		*/

		foreach ($questionAnswers as $questionAnswer)
		{
			// counts the amount of answers for each question
			// used for calculating percentage
			// 
			if (!array_key_exists($questionAnswer->question_id, $questionCount)) {
				$questionCount[$questionAnswer->question_id] = 1;
			}
			else {
				$questionCount[$questionAnswer->question_id]++;
			}

			// counts the amount of times each option was chosen
			if (!array_key_exists($questionAnswer->option_id, $answerInfo)) {
				$answerInfo[$questionAnswer->option_id] = 1;
			}
			else {
				$answerInfo[$questionAnswer->option_id]++;
			}
		}

		$answerInfo['question_count'] = $questionCount;

		return $answerInfo;
	}

}
